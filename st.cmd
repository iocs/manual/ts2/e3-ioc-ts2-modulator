require modbus
require essioc
require modulator

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "TS2-010RFC:SC-IOC-004")
epicsEnvSet(IOCDIR, "TS2-010RFC_SC-IOC-004")
epicsEnvSet(LOG_SERVER_NAME, "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# Load MODULATOR module
############################################################################
epicsEnvSet(SYS, "TS2")
epicsEnvSet(SUB, "010")
epicsEnvSet(ID, "001")
epicsEnvSet(MOD_IP, "172.16.110.82")
#-epicsEnvSet(PSS_PV, "FEB-010Row:CnPw-U-004:ToRFQLPS")
iocshLoad("$(modulator_DIR)/modulator.iocsh", "SYS=$(SYS), SUB=$(SUB), ID=$(ID), MOD_IP=$(MOD_IP)")

############################################################################
# IOC Startup
############################################################################
iocInit()

